# SPARQL Test

## Automatic testing of SPARQL queries with Gitlab CI

A Python script that loads SPARQL queries from a configured source, loads test data from a configured source, executes queries against test data, compares with expected results and reports the outcome.

Configured sources can be local folders or folders in remote repositories.

### Execution

`sparql-test.py` requires 2 command-line arguments:

1.  First argument is the absolute path to the source folder from where the SPARQL queries are to be retrieved
    -  e.g. `C:/path/to/queries/adm/lc-uv/WD/2020/06`
2.  Second argument is the SPARQL endpoint that the queries are executed against
    -  e.g. `http://localhost:3030/sparql-test/`
3.  Optional third argument is the absolute path to a JSON file specifiying a list of artifacts with test data that are to be queried against
    -  e.g. `{ "artifacts": [ "C:/path/to/adm/lc-uv/2020/03/instance-data.ttl", "http://purl.allotrope.org/voc/afo/merged/CR/2020/03/merged-and-inferred.ttl" ] }`
    -  e.g. `{ "artifacts": [ "C:/path/to/adm/lc-uv/2020/03/instance-data.ttl", "http://purl.allotrope.org/voc/afo/merged/CR/2020/03/merged-and-inferred.ttl", "`string of properly escaped triples`" ] }`

Helge Krieg, OSTHUS 2020