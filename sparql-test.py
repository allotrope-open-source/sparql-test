# SPARQL Test
# Loads SPARQL queries from configured source, loads test data from configured source, executes queries against test data and compares with expected results.
# Requires 2 command-line arguments:
# 1. Absolute path to source folder to load queries from
#		e.g. C:/path/to/queries/adm/lc-uv/WD/2020/06
# 2. SPARQL endpoint
#		e.g. http://localhost:3030/sparql-test/
# Optionally, provide a third argument:
# 3. Absolute path to JSON file specifiying list of artifacts with test data that are to be queried against
#		e.g. { "artifacts": [ "C:/path/to/adm/lc-uv/2020/03/instance-data.ttl", "http://purl.allotrope.org/voc/afo/merged/CR/2020/03/merged-and-inferred.ttl" ] }
#
# Helge Krieg, OSTHUS 2020

import unittest
import os
import json
import pprint
import io
import platform
import sys
import requests
import deepdiff
import xmlrunner
from xmlrunner.extra.xunit_plugin import transform
import wget
import urllib

version='0.1.0'
temp_download_file = 'temp.ttl'
query_url = "query"
update_url = "update"
data_url = "data?default"

sparql_count_all = 'SELECT COUNT(*) WHERE { ?s ?p ?o }'
sparql_delete_all = 'DELETE WHERE { ?s ?p ?o }'
upload_headers = {'Content-Type': 'text/turtle;charset=utf-8'}

class SparqlTest(unittest.TestCase):
	def is_url(url):
		try:
			result = urllib.parse.urlparse(url)
			if all([result.scheme, result.hostname, result.netloc]):
				return True
			else:
				return False
		except:
			return False

	def reset_triple_store(endpoint):
		query_endpoint = endpoint + query_url
		update_endpoint = endpoint + update_url
		print("Reset triple store at endpoint: %s" % (update_endpoint))
		response = requests.post(query_endpoint, data={'query': sparql_count_all})
		print("Number of triples before resetting triple store\n" + response.text)
		response = requests.post(update_endpoint, data={'update': sparql_delete_all})
		print("Response of delete\n" + response.text)
		response = requests.post(query_endpoint, data={'query': sparql_count_all})
		print("Number of triples after resetting triple store\n" + response.text)
		return

	def load_artifacts(endpoint, artifacts):
		query_endpoint = endpoint + query_url
		data_endpoint = endpoint + data_url
		print("Load data to endpoint: %s" % (data_endpoint))
		response = requests.post(query_endpoint, data={'query': sparql_count_all})
		print("Number of triples before loading artifacts\n" + response.text)

		for artifact in artifacts:
			if SparqlTest.is_url(artifact):
				print("Downloading artifact from remote location: " + artifact)
				if os.path.exists(os.path.abspath(temp_download_file)):
					os.remove(os.path.abspath(temp_download_file))
				wget.download(artifact, os.path.abspath(temp_download_file))
				artifact = os.path.abspath(temp_download_file)
				artifact_data = open(artifact, 'r', encoding='utf-8').read()
			elif os.path.isfile(artifact):
				print("Loading artifact from local file: " + artifact)
				artifact_data = open(artifact, 'r', encoding='utf-8').read()
			else:
				print("Loading data from config file directly to triple store: \n" + artifact)
				artifact_data = artifact
			response = requests.post(data_endpoint, data=artifact_data.encode('utf-8'), headers=upload_headers)
			print("Response of loading data\n" + response.text)

			response = requests.post(query_endpoint, data={'query': sparql_count_all})
			print("Number of triples after loading artifact\n" + response.text)
		return

	def create_test_method(testcase):
		def test(self):
			name = testcase['name']
			query_endpoint = testcase['endpoint'] + query_url
			query = testcase['query']
			expected_result = testcase['expected_result']
			artifacts = testcase['artifacts']
			load_artifacts = testcase['override_global_artifacts']

			print("Testname: %s" % (name), flush=True)
			#print("Query: %s" % (query), flush=True)
			print("Endpoint: %s" % (query_endpoint), flush=True)
			print("Expected result: %s" % (expected_result), flush=True)
			test_data_artifacts = []
			for artifact in artifacts:
				if SparqlTest.is_url(artifact) or os.path.isfile(artifact):
					test_data_artifacts.append(artifact)
				else:
					test_data_artifacts.append("additional triples from config file")

			print("Test data from: %s" % (json.dumps(test_data_artifacts)))

			if load_artifacts:
				print('Loading specific artifacts: ' + json.dumps(artifacts))
				SparqlTest.reset_triple_store(testcase['endpoint'])
				SparqlTest.load_artifacts(testcase['endpoint'], artifacts)

			response = requests.post(query_endpoint, data={'query': query})
			actual_result = None
			if (response.status_code == 200):
				print("Response 200\n" + response.text)
				actual_result = json.loads(response.text)
			elif (response.status_code == 400):
				print("Response 400\n" + response.text)
			else:
				raise Exception('Unhandled response')

			if actual_result != None:
				diff = deepdiff.DeepDiff(expected_result, actual_result, ignore_order=True)
				if (diff == {}):
					return

				pp = pprint.PrettyPrinter(4)
				print("Expected result:")
				pp.pprint(expected_result)
				print("Actual result:")
				pp.pprint(actual_result)
				raise Exception("Actual result differs from expected result.")
			else:
				raise Exception('No result found!')
		return test


if __name__ == '__main__':
	print("Executing SPARQL-Test version: " + version)

	target_folder = ''
	target_endpoint = ''
	global_artifacts_file_name = ''

	if len(sys.argv) == 4:
		global_artifacts_file_name = sys.argv.pop()
		target_endpoint = sys.argv.pop()
		target_folder = sys.argv.pop()
	elif len(sys.argv) == 3:
		target_endpoint = sys.argv.pop()
		target_folder = sys.argv.pop()
	else:
		print('ERROR: sparql-test requires exactly 2 arguments: 1. the absolute path to the test folder 2. URL of the SPARQL Endpoint. Optionally provide a filename as third argument in order to provide test data to be loaded and queried')
		sys.exit(1)

	print("Loading test data from folder: " + target_folder)
	print("Testing against SPARQL endpoint: " + target_endpoint)

	global_artifacts = None
	if global_artifacts_file_name != '':
		print("Loading artifacts from configuration file: " + global_artifacts_file_name)
		with open(global_artifacts_file_name, 'r', encoding='utf-8') as global_artifacts_file:
			artifacts_configuration = json.load(global_artifacts_file)
			global_artifacts = artifacts_configuration['artifacts']
		print("Loading RDF test data from globally defined sources: " + json.dumps(global_artifacts))

	directory = os.fsencode(target_folder)

	testcases = []
	for file in os.listdir(directory):
		filename = os.fsdecode(file)
		if filename.lower().endswith(".rq") or filename.lower().endswith(".sparql"):
			print("Loading query from file: " + filename)
			query = None
			with open(target_folder + os.sep + filename, 'r', encoding='utf-8') as queryfile:
				query = queryfile.read()

			testconfig = filename.lower().replace('.sparql','-testconfig.json').replace('.rq','-testconfig.json')
			artifacts = global_artifacts
			override_global_artifacts = False
			expected_result = None
			if os.path.exists(target_folder + os.sep + testconfig):
				print(target_folder + os.sep + testconfig)
				with open(target_folder + os.sep + testconfig, 'r', encoding='utf-8') as configfile:
					configuration = json.load(configfile)
					expected_result = configuration['expected_result']
					if configuration.get('artifacts') != None:
						artifacts = configuration['artifacts']
						print("Loading artifacts from specifically defined sources: " + json.dumps(configuration['artifacts']))
						override_global_artifacts = True

			if artifacts == None:
				print('ERROR: Please specify URL of test data that is to be loaded and queried.')
				sys.exit(1)

			testcase = {
				"name" : filename.lower().replace('.rq', '').replace('.sparql', '') ,
				"input_file" : filename ,
				"expected_file" : testconfig,
				"query" : query,
				"expected_result" : expected_result,
				"endpoint" : target_endpoint,
				"artifacts" : artifacts,
				"override_global_artifacts" : override_global_artifacts
			}

			testcases.append(testcase)

	for testcase in testcases:
		test_query = SparqlTest.create_test_method(testcase)
		setattr(SparqlTest, 'test_%s' % (testcase['name']), test_query)

	SparqlTest.reset_triple_store(target_endpoint)
	if global_artifacts != None:
		print('Loading globally specified artifacts: ' + json.dumps(artifacts))
		SparqlTest.load_artifacts(target_endpoint, global_artifacts)

	# unittest.main(verbosity=2)

	out = io.BytesIO()
	result = unittest.main(testRunner=xmlrunner.XMLTestRunner(output=out), failfast=False, buffer=False, catchbreak=False, exit=False)
	with open('sparql-test-report.xml', 'wb') as report:
		report.write(transform(out.getvalue()))

	if len(result.result.errors) == 0 and len(result.result.failures) == 0:
		exit(0)
	exit(1)
